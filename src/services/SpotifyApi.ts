import StorageManager from "../app/storageManager";

const CALLBACK_PATH = "/aSpotCallback/";
const ApiAuthStorage = new StorageManager("ApiSFAuth", {});
var currentUserInfoPromise: any = null;
/**
 * get the params of the location to get saved
 * @returns any
 */
function getHashParams() {
    var hashParams: any = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    while (e = r.exec(q)) {
        hashParams[e[1]] = decodeURIComponent(e[2]);
    }
    return hashParams;
}

/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */
function generateRandomString(length: number) {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
};

/**
 *  call the login on spotify prepare page with / several login options.
 */
export function doLoginOnSpotify() {
    var client_id = '9f7fba62c89641829ebac61ad6f6121b'; // Your client id
    var redirect_uri = window.location.protocol + "//" + window.location.host + CALLBACK_PATH; // Your redirect uri
    var state = generateRandomString(16);
    ApiAuthStorage.set({
        state,
        frompath: window.location.href
    });

    var scope = 'user-top-read user-library-modify';

    var url = 'https://accounts.spotify.com/authorize' + '?response_type=token' + '&client_id=' + encodeURIComponent(client_id)
        + '&scope=' + encodeURIComponent(scope)
        + '&redirect_uri=' + encodeURIComponent(redirect_uri)
        + '&state=' + encodeURIComponent(state) + "&show_dialog=true";

    window.location.href = url;

}



/**
 *  whenever it get here is becouse a response with the auth token is been returned
 */
export function catchSpotifyAuth() {
    if (window.location.pathname === CALLBACK_PATH) {
        /**
         *  cath the Auth token for this app
         */
        var params = getHashParams();
        if (ApiAuthStorage.data.state === params.state) {
          
            ApiAuthStorage.set({
                access_token: params.access_token,
                starttime: Date.now(),
                expires_in: params.expires_in
            });
            // JUST RELOAD THE PAGE WHERE I WAS SUPPOSE TO GO.     

            return true;

        }
    }

    return false;
}

export function reloadPreviusPath() {
    window.location.href = ApiAuthStorage.data.frompath;
}

export function getTokenStartTime(): number {
    return ApiAuthStorage.data.starttime;
}
export function getSecondsLeft() {
    const getSecondsPass = (startTime: number) => {
        let endTime = Date.now();
        var timeDiff = (endTime - startTime) / 1000; //in ms 
        // get seconds 
        var seconds = Math.round(timeDiff);
        return seconds;
    };
    return ((ApiAuthStorage.data.expires_in || 3600) - getSecondsPass(getTokenStartTime()));
}
/**
 *  this method is used to do an api call to spotify webservice
 * @param operation 
 * @returns 
 */
function apiCall(operation: String) {
    if (!isAuthenticatedOnSpotify()) {
        return Promise.reject("not authenticated");
    }
    return fetch('https://api.spotify.com/v1/' + operation, {
        method: "get",
        headers: {
            'Authorization': 'Bearer ' + ApiAuthStorage.data.access_token
        }
    }).then(r => r.json())
}
/**
 * 
 * @returns returns true or false
 */
export function isAuthenticatedOnSpotify() {
    if (!ApiAuthStorage.data.access_token) {
        catchSpotifyAuth();
    }
    return ApiAuthStorage.data.access_token;

}
/**
 * get the user spotify user info.
 * @returns 
 */
export function getSpotifyUserInfo() {

    if (currentUserInfoPromise) {
        return currentUserInfoPromise;
    }
    return currentUserInfoPromise = apiCall("me").then((r) => {
        currentUserInfoPromise = null;
        return r;
    })
}


/** 
 * @returns 
 */
export function getSpotifyCategories(offset = 0, limit = 10, market = "ES") {
    return apiCall(`browse/categories?market=${market}&offset=${offset}&limit=${limit}`);
}


/** 
 * @returns 
 */
export function getSpotifyCategoriesPlayList(categoryId: string, offset = 0, limit = 5, market = "ES") {
    return apiCall(`browse/categories/${categoryId}/playlists?market=${market}&offset=${offset}&limit=${limit}`);
}


export function getSpotifyTracksFromPlayList(playlist_id: string, offset = 0, limit = 5, market = "ES") {
    return apiCall(`playlists/${playlist_id}/tracks?market=${market}&offset=${offset}&limit=${limit}`)
}

/**
 *  clear session to logout
 */
export function clearSpotifySession() {
    ApiAuthStorage.clear();
}
