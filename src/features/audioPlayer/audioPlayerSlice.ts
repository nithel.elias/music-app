import { createSlice } from "@reduxjs/toolkit";
import StorageManager from "../../app/storageManager";
import { RootState } from "../../app/store";

class AudioWrapper {
    audio: HTMLAudioElement;
    state: 'idle' | 'ready' | 'playing' | 'loading' | 'failed';
    src: string;
    private callbackonload: () => void;
    private callbackonplaying: () => void;
    constructor() {
        this.audio = new Audio();
        this.state = "idle";
        this.src = "";
        this.callbackonload = () => { };
        this.callbackonplaying = () => { };
        this.audio.onload = () => {
            if (this.audio.paused) {
                this.state = "ready";
            }
            this.callbackonload();
        };
        this.audio.onerror = () => {
            this.state = "failed";
        };
        this.audio.ontimeupdate = () => {
            this.state = "playing";
            this.callbackonplaying();
        };

    }
    onLoad(_callbackload: () => void) {
        this.callbackonload = _callbackload
    }
    onPlaying(_callback: () => void) {
        this.callbackonplaying = _callback;
    }
    isPlaying() {
        return !this.audio.paused;
    }
    set(url: string) {
        if (this.src !== url) {
            this.state = "loading";
        }
        this.src = url;
        this.audio.src = url;
        if (url) {
            this.audio.play();
        }
    }
    play() {
        this.audio.play();
    }
    pause() {
        this.audio.pause();
    }
}
type audioPlayerType = {
    src: string,
    state: 'idle' | 'ready' | 'playing' | 'loading' | 'failed';
};

const initialState: audioPlayerType = {
    src: "",
    state: 'idle',
};


const currentAudio = new AudioWrapper();

const audioPlayerSlice = createSlice({
    name: 'audioPlayer',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        setTrack: (state, action) => {
            state.src = action.payload.track;
            state.state = action.payload.state;

        }
    }

});

export const toggleTrack = (trackurl: string): Promise<'idle' | 'ready' | 'playing' | 'loading' | 'failed'> => {
    return new Promise((resolve) => {
        if (!trackurl) {
          
            currentAudio.state = "failed";
            currentAudio.pause();
            currentAudio.src = currentAudio.audio.src = "";
            resolve(currentAudio.state);
            return
        }
        if (currentAudio.src !== trackurl) {
            currentAudio.state = "ready";
            currentAudio.set(trackurl);
        } else {
            if (currentAudio.isPlaying()) {
                currentAudio.state = "ready";
                currentAudio.pause();
            } else {
                currentAudio.state = "playing";
                currentAudio.play();
            }


        }
        resolve(currentAudio.state);

    });
};

export const getAudioPlayer = (): AudioWrapper => {
    return currentAudio
}

export const { setTrack } = audioPlayerSlice.actions;

export const selectAudioPlayer = (state: RootState) => state.audioplayer;

export default audioPlayerSlice.reducer;