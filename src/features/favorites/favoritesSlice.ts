import { createSlice } from "@reduxjs/toolkit";
import StorageManager from "../../app/storageManager";
import { RootState } from "../../app/store";

type favoritesStateType = {
    favorites: {
        [id: string]: number
    };
    status: 'idle' | 'loading' | 'failed';
};
const favoritesStorage = new StorageManager("favorites", { favorites: {} });
const initialState: favoritesStateType = {
    favorites: favoritesStorage.data.favorites,
    status: 'idle',
};


const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        addTrackToFavorite: (state, action) => {
            // ADD A FAVORITE TO THE TRACK 
            state.favorites[action.payload.trackid] = 1;
        },
        removeTrackFromFavorite: (state, action) => {
            // ADD A FAVORITE TO THE TRACK
            let temp = { ...state.favorites };
            delete temp[action.payload.trackid];
            state.favorites = { ...temp }
        }
    }

});

export const { addTrackToFavorite, removeTrackFromFavorite } = favoritesSlice.actions;

export const selectFavorites = (state: RootState) => state.favorites.favorites;

export const addTrackToFavoriteOnStore = (trackId: string) => {

    favoritesStorage.set({
        favorites: {
            ...favoritesStorage.data.favorites,
            [trackId]: 1
        }
    });
}




export const removeTrackFromFavoriteOnStore = (trackId: string) => {
    let temp = { ...favoritesStorage.data.favorites };
    delete temp[trackId];
    favoritesStorage.set({
        favorites: {
            ...temp
        }
    });


};

export default favoritesSlice.reducer;