import { createSlice } from "@reduxjs/toolkit";
import StorageManager from "../../app/storageManager";
import { RootState } from "../../app/store";
import { clearSpotifySession, getSpotifyUserInfo } from "../../services/SpotifyApi";
type ImageProfileType = {
    url: string
};
type userType = {
    id: string,
    display_name: string,
    email: string,
    images: Array<ImageProfileType>
}
export interface UserState {
    user: userType | any;
    status: 'idle' | 'loading' | 'failed';
};


const userStorage = new StorageManager("userinfoApp");

const initialState: UserState = {
    user: userStorage.data,
    status: 'idle',
};


const userSlice = createSlice({
    name: 'user',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        setUser: (state, action) => {
            state.user = action.payload;
        }
    }

});


export async function getUserInfo() {
    if (userStorage.data == null) {
        let response = await getSpotifyUserInfo();
        userStorage.set(response);
    }
    return userStorage.data;
}

export async function clearSession() {
    clearSpotifySession();
    userStorage.clear();
}



export const { setUser } = userSlice.actions;
export const selectUser = (state: RootState) => state.user.user;

export default userSlice.reducer;