import userReducer, { setUser, selectUser, UserState } from "./userSlice";

describe('validate user', () => {
    const nitheluser = {
        id: "1",
        display_name: "nithel",
        email: "nithel.elias@gmail.com",
        images: [{ url: "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=10203119836209515&height=300&width=300&ext=1659273803&hash=AeTPdXkjDRQh3SjGRcM" }]
    }
    const initialState: UserState = {
        user: {},
        status: 'idle',
    };
    it('should handle initial state', () => {
        expect(userReducer(undefined, { type: 'unknown' })).toEqual({
            user: {},
            status: 'idle',
        });
    });
});