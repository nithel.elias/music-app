import { createSlice } from "@reduxjs/toolkit";
import StorageManager from "../../app/storageManager";
import { RootState } from "../../app/store";
import { getSpotifyCategoriesPlayList } from "../../services/SpotifyApi";


export type PlayListType = {
    id: string,
    name: string,
    description: string,
    image: string,
    tracks: string,
    track_list: Array<string>
};

type PlaylistsStateType = {
    playlists: {
        [id: string]: Array<string>
    };
    playlistDic: {
        [id: string]: PlayListType
    },
    status: 'idle' | 'loading' | 'failed';
};

const playListStorage = new StorageManager("playlists", { playlists: {}, playlistDic: {} });
const initialState: PlaylistsStateType = {
    playlists: playListStorage.data.playlists,
    playlistDic: playListStorage.data.playlistDic,
    status: 'idle',
};

const playListsSlice = createSlice({
    name: 'playlists',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        addPlaylists: (state, action) => {
            state.playlists[action.payload.categoryId] = action.payload.playlists;
            state.playlistDic = {
                ...state.playlistDic,
                ...action.payload.playlistDic
            }
        },
        addTracksToPlayList: (state, action) => {
            state.playlistDic[action.payload.playlist_id].track_list = action.payload.track_list
        }
    }

});


export type getPlayListsReturnType = {
    list: Array<string>,
    dic: {
        [id: string]: PlayListType
    }
};
export async function requestPlayLists(categoryId: string, total = 0): Promise<getPlayListsReturnType> {

    let dic: any = {};
    let list: Array<string> = [];
    let response = (await getSpotifyCategoriesPlayList(categoryId, 0, total));
    if (!response) {
        return { list, dic };
    }
    // Nithel: LETS DO A MAPPING TO ENSURE ONLY THE DATA WE REALLY NEED TO THIS SIMPLE APP 
    response = response.playlists.items.map((record: any) => {
        return {
            id: record.id,
            name: record.name,
            description: record.description,
            image: record.images[0].url,
            tracks: record.tracks.href
        }
    });
    let tempstore = {
        playlists: {
            ...playListStorage.data.playlists,

        },
        playlistDic: {
            ...playListStorage.data.playlistDic,
        }
    };

    tempstore.playlists[categoryId] = list = response.map((playlist: PlayListType) => {
        tempstore.playlistDic[playlist.id] = dic[playlist.id] = playlist;
        return playlist.id
    });
    playListStorage.set(tempstore);

    return { list, dic };
}



export const { addPlaylists, addTracksToPlayList } = playListsSlice.actions;

export const selectPlayLists = (state: RootState) => state.playlists.playlists;
export const selectPlayListDic = (state: RootState) => state.playlists.playlistDic;


export default playListsSlice.reducer;