import { createSlice } from "@reduxjs/toolkit";
import StorageManager from "../../app/storageManager";
import { RootState } from "../../app/store";
import { getSpotifyTracksFromPlayList } from "../../services/SpotifyApi";

export type AlbumType = {
    id: string,
    name: string,
    image: string,
    href: string
};

export type TrackType = {
    id: string
    name: string,
    href: string,
    mp3: string,
    uri: string,
    duration_ms: number,
    album: AlbumType,
    artists: Array<string>
};

type TracksStateType = {
    tracks: {
        [id: string]: any
    };
    status: 'idle' | 'loading' | 'failed';
};

const tracksStorage = new StorageManager("tracks", { tracks: {} });
const initialState: TracksStateType = {
    tracks: tracksStorage.data.tracks,
    status: 'idle',
};

const tracksSlice = createSlice({
    name: 'tracks',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        addTracks: (state, action) => {

            action.payload.tracks.forEach((track: TrackType) => {
                state.tracks[track.id] = track;
            });
        }
    }

});



export async function getTracksFromPlayList(playlist_id: string): Promise<Array<TrackType>> {


    let response = (await getSpotifyTracksFromPlayList(playlist_id, 0, 15));
    if (!response) {
        return []
    }
    let currentTracks = { ...tracksStorage.data.tracks };
    // Nithel: LETS DO A MAPPING TO ENSURE ONLY THE DATA WE REALLY NEED TO THIS SIMPLE APP 
    response = response.items.map((record: any) => {
        let track = record.track;
       
        let newTrack = {
            id: track.id,
            name: track.name,
            href: track.href,
            mp3: track.preview_url,
            uri: track.uri,
            duration_ms: track.duration_ms,
            artists: track.artists.map((artist: any) => artist.name),
            album: {
                id: track.album.id,
                name: track.album.name,
                image: track.album.images[0].url,
                href: track.album.href
            }
        };
        currentTracks[newTrack.id] = newTrack;
        return newTrack;
    });

    tracksStorage.set({
        tracks: {
            ...currentTracks
        }
    });


    return response;
}





export const { addTracks } = tracksSlice.actions;

export const selectTracks = (state: RootState) => state.tracks.tracks;


export default tracksSlice.reducer;