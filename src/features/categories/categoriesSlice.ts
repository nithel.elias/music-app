import { createSlice } from "@reduxjs/toolkit";
import StorageManager from "../../app/storageManager";
import { RootState } from "../../app/store";
import { getSpotifyCategories } from "../../services/SpotifyApi";

export type CategoriesType = {
    id: string,
    name: string,
    image: string,
};

export type CategoriessState = {
    list: Array<any>;
    page: number;
    status: 'idle' | 'loading' | 'failed';
};

const categoryListDic: any = {

};
const categoriesStorage = new StorageManager("categories", { categories: [] });
const initialState: CategoriessState = {
    list: categoriesStorage.data.categories,
    page: 0,
    status: 'idle',
};

const categoriesSlice = createSlice({
    name: 'categories',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        addCategories: (state, action) => {

            state.list = state.list.concat(action.payload.categories);
            state.page = action.payload.page;

        } 
    }

});

function fillCategoryListDic(categoriesList: Array<CategoriesType>) {
    categoriesList.forEach((category: CategoriesType) => {
        categoryListDic[category.id] = 1;
    });
}
export async function getCategories(page = 0, limit = 10): Promise<Array<CategoriesType>> {


    // Nithel: LETS DO A MAPPING TO ENSURE ONLY THE DATA WE REALLY NEED TO THIS SIMPLE APP 
    let response = (await getSpotifyCategories(limit * page, limit)).categories.items.map((record: any) => {
        return {
            id: record.id,
            name: record.name,
            image: record.icons[0].url,
        }
    })
        // FOR SOME REASON THE SPOTIFY CATEGORIES RETURN SAME LISTS ON DIFERENTES PAGES
        .filter((category: CategoriesType) => {
            return !categoryListDic.hasOwnProperty(category.id)
        });
    fillCategoryListDic(response);
    // ONLY STORE THE FIRST DATA
    if (!categoriesStorage.data.initiated) {
        categoriesStorage.set({ initiated: true, page, categories: response });
    }

    return response;
};




export const { addCategories } = categoriesSlice.actions;

export const selectCategories = (state: RootState) => state.categories;

export default categoriesSlice.reducer;