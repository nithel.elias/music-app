import './App.css';
import { useAppDispatch, useAppSelector } from './app/hooks';
import Home from './components/pages/Home/Home';
import Loading from './components/pages/Loading/Loading';
import Login from './components/pages/Login/Login';
import { selectUser, getUserInfo, setUser } from './features/user/userSlice';
import { catchSpotifyAuth, isAuthenticatedOnSpotify } from './services/SpotifyApi';




function App() {
  const user = useAppSelector(selectUser);
  const dispatch = useAppDispatch();


  if (catchSpotifyAuth()) {
    window.location.href = "/";
    return <Loading />;
  }


  // with this i validate if in reality im authenticated with spotify
  if (isAuthenticatedOnSpotify()) {
    if (!user) {
      getUserInfo().then((datauser) => {
        dispatch(setUser(datauser));
      });
      return <Loading />;
    }

    return (
      <Home></Home>
    );
  }
  // do login if not authenticated.
  return (
    <Login ></Login>
  );
}

export default App;
