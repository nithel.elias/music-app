
import { useEffect, useRef } from 'react';
import { APPNAME } from '../../../AppConfig';
import { doLoginOnSpotify } from '../../../services/SpotifyApi';
import AppButton from '../../atoms/AppButton/AppButton';
import LogoAppImg from '../../atoms/LogoAppImg/LogoAppImg';

import './style.css';

export default function Login() {
    const viodeRef = useRef<HTMLVideoElement>(null);
    const playVideo = () => {
        if (viodeRef.current != null) {
            viodeRef.current.play();
        }
    }
    const toggleVideo = () => {
        if (viodeRef.current != null) {
            if (viodeRef.current.paused) {
                viodeRef.current.play();
            } else {
                viodeRef.current.pause();
            }
        }
    }
    useEffect(() => {

        playVideo();

    });
    return <main className='page__login'>
        <div className='login--back' >
            <video className='login--video' ref={viodeRef} loop>
                <source src="/video/intro.mp4"></source>
                no se puede poner el video aqui
            </video>
        </div>
        <div className='login--front'>
            <div className='login__container' onClick={() => {
                toggleVideo();
            }}>
                <div className='login__container--top'>

                    <figure className='login__figure' >
                        <LogoAppImg></LogoAppImg>
                    </figure>
                    <h1 className='login--tittle'>
                        {APPNAME.toUpperCase()}
                    </h1>
                </div>
                <div className='login__container--bottom'>
                    <AppButton id="login-button" className={"btn btn-primary"}
                        onclick={(evt: Event) => {
                            doLoginOnSpotify();
                            evt.preventDefault();
                        }}
                    >Login with Spotify user</AppButton>
                </div>
            </div>
        </div>
    </main>
}