import { useEffect, useState } from "react";
import { doLoginOnSpotify } from "../../../services/SpotifyApi";
import "./style.css";
export default function ReloginInnerPage() {
    let [time, setTime] = useState(2);


    useEffect(() => {
        if (time <= 0) {
            doLoginOnSpotify();
        } else {
            let intervalId = setTimeout(() => {
                setTime(time - 1);
            }, 1000);
            return () => {
                clearTimeout(intervalId)
            }
        }


    })
    return <div className='innerpage__relogin'>
        <article className="innerpage__relogin__body">
            <h2 className="fa fa-clock-o" ></h2>
            <h3>Se ha acabado el tiempo ahora toca re logearse!</h3>
            <p>relogin in: {time}s</p>
        </article>
    </div>
}