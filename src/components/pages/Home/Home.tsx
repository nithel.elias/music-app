import { getCleanHash, useHash } from "../../../app/useHash";
import HeaderApp from "../../organism/HeaderApp/HeaderApp";
import ExploreInnerPage from "../ExploreInnerPage/ExploreInnerPage";

import ProfileCardView from "../../organism/ProfileCardView/ProfileCardView";
import TimerApp from "../../organism/TimerApp/TimerApp";
import { initValidRoutes, routeApp } from "../../../app/routeApp";
import ReloginInnerPage from "../ReloginInnerPage/ReloginInnerPage";
import "./style.css";
initValidRoutes(["/"], ["browse", "favorites", "/*/profile", "relogin", "category/*", "playlist/*"]);

export default function Home() {
    const [hash, setHash] = useHash();
    const currentHashview = getCleanHash();
    routeApp();
    /// HERE MUST BE SOME SERVICE OBJECT WITH THE FEEDs    
    return (<main className="page__home">
        <HeaderApp  ></HeaderApp>
        <article className="page__home_body">
            {currentHashview === "relogin" ? <ReloginInnerPage></ReloginInnerPage> : null}
            {
                currentHashview.indexOf("profile") > -1 ? <ProfileCardView></ProfileCardView> : null
            }
            <ExploreInnerPage currentview={currentHashview}></ExploreInnerPage>

        </article>
        <footer  >
            <TimerApp></TimerApp>
        </footer>
    </main>);
}