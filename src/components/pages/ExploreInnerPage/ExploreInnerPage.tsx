

import CategoriesView from "../../organism/CategoriesView/CategoriesView";
import FavoritesView from "../../organism/FavoristView/FavoritesView";
import PlayListView from "../../organism/PlayListView/PlayListView";
import "./style.css";

type propsInnerPage = {
    currentview: string
};
export default function ExploreInnerPage({ currentview = "" }: propsInnerPage) {


    return (<div className="innerpage__explore">
        {currentview.indexOf("browse") > -1 ? <CategoriesView></CategoriesView> : null}
        {currentview.indexOf("favorites") > -1 ? <FavoritesView></FavoritesView> : null}
        {currentview.indexOf("playlist") > -1 ? <PlayListView playlist_id={currentview.split("playlist/")[1].split("/")[0]} /> : null}

    </div>)
}