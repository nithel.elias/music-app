
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { addPlaylists, getPlayListsReturnType, requestPlayLists, selectPlayListDic, selectPlayLists } from "../../../features/playlist/playlistSlice";
import CardItem from "../../molecules/CardItem/CardItem";
import "./style.css";

interface propsCategoryPlayLists {
    categoryId: string;
    getTotalTracks: (total: number) => void;
};

/**
 *  TO AVOID MULTIPLE REFRESH ON THE VIEW
 */
const pruns: any = {};

export default function CategoryPlayLists({ categoryId, getTotalTracks }: propsCategoryPlayLists) {
    if (!pruns[categoryId]) {
        pruns[categoryId] = {
            runs: 0,
            state: "idle"
        };
    }

    const playListsStore = useAppSelector(selectPlayLists);
    const playListDicStore = useAppSelector(selectPlayListDic);
    const dispatch = useAppDispatch();
    const playlists = (playListsStore[categoryId] || []);
    (() => {
        if (playlists.length > 0) {
            return;
        }
        if (pruns[categoryId].state != "idle") {
            return;
        }
        pruns[categoryId].runs += 1;
        pruns[categoryId].state = "running";

        requestPlayLists(categoryId, 6).then((response: getPlayListsReturnType) => {
            pruns[categoryId].state = "idle";
            if (response.list.length === 0) {
                // NO TRACKS HERE 
                getTotalTracks(0);
                return
            }
            if (pruns[categoryId].runs === 1) {
                dispatch(addPlaylists({ categoryId, playlists: response.list, playlistDic: response.dic }));
            }
        }, () => {
            // NOT FOUND EITHER
            getTotalTracks(0);
        });
    })();

    return <div className="category__playlist category__playlist--grid">

        {playlists.length == 0 && pruns[categoryId].state === "running" ? <div className="category__playlist__lodingwrapper">Cargando <i className="fa fa-spinner fa-spin"></i></div> : null}
        {playlists.map((playlistId: string, idx: number) => {
            if (!playListDicStore.hasOwnProperty(playlistId)) {
                return null;
            }
            const playlist = playListDicStore[playlistId];
            return <CardItem key={idx}
                tittle={playlist.name}
                subtittle={playlist.description}
                imageUrl={playlist.image}
                onclick={() => {
                    window.location.hash = "/playlist/" + playlist.id;
                }}></CardItem>;
        })}
    </div>
}