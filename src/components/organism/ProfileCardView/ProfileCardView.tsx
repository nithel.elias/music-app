import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { clearSession, selectUser, setUser } from "../../../features/user/userSlice"; 
import AppButton from "../../atoms/AppButton/AppButton";
import ProfileCirclePic from "../../atoms/ProfileCirclePic/ProfileCirclePic";
import "./style.css";
export default function ProfileCardView() {
    const user = useAppSelector(selectUser);
    const dispatch = useAppDispatch();
    const closeSessionHandlerClick = () => {
        clearSession();
        dispatch(setUser(null));
        window.location.href = "";
    };
    return (<div className="profilecardview">
        <div className="profilecardview_container">
            <a className="profilecardview--close fa fa-close" onClick={()=>{
                window.history.back();   
            }}></a> 
            <figure className="profilecardview__figure">
                <ProfileCirclePic />
            </figure>
            <article className="profilecardview__body">
                <p>{user.display_name}</p>
                <p className="profilecardview__body__text--light"><small>{user.email}</small></p>
                <br></br>
            </article>
            <section className="profilecardview__footer">
                <AppButton onclick={closeSessionHandlerClick}>
                    Cerrar Sesión
                </AppButton>
            </section>
        </div>
    </div>)
}