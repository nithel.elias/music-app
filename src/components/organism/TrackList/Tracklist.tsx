import { useAppSelector } from "../../../app/hooks";

import { selectTracks, TrackType } from "../../../features/tracks/tracksSlice";
import TrackCard from "../../molecules/TrackCard/TrackCard";

import "./style.css";
type propsTrackList = {
    track_list: Array<string>
}
export default function TrackList({ track_list }: propsTrackList) {
    const trackStore = useAppSelector(selectTracks);

    return <div role={"presentation"} className="trackList">
        {
            track_list.map((trackId: string, idx: number) => {
                if (!trackStore.hasOwnProperty(trackId)) {
                    return null;
                    //<div>notfound..({trackId})</div>;
                }
                return <TrackCard track={trackStore[trackId]} key={idx}></TrackCard>;
            })
        }
    </div>;
}