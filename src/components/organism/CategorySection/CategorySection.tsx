
import { useState } from "react";
import { CategoriesType } from "../../../features/categories/categoriesSlice";
import AppLink from "../../atoms/AppLink/AppLink";
import SubtitleAndDescription from "../../molecules/SubTitleAndDescription/SubtitleAndDescription";
import CategoryPlayLists from "../CategoryPlayLists/CategoryPlayLists";
import "./style.css";

type propsCategory = {
    category: CategoriesType
};


export default function CategorySection({ category }: propsCategory) {
    const [avaible, setAvaible] = useState(true);
    if (!avaible) {
        return null;
    }
    const gettotalTracksCallback = (total: number) => {
        if (total === 0) {
            setAvaible(false);
        }
    };
    return <section className="categories__section">
        <div className="categories__section__header">
            <SubtitleAndDescription tittle={<a className="no-link-style" href={"category/" + category.id}>{category.name}</a>} description=""
                className="categories__section__header_title" ></SubtitleAndDescription>
            <AppLink className="categories__section__header_linktoall" href={"category/" + category.id} />
        </div>
        <div className="categories__section_row__body">
            <CategoryPlayLists categoryId={category.id} getTotalTracks={gettotalTracksCallback} />
        </div>
    </section>;
}