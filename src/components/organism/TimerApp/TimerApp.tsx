import { useEffect, useState } from "react"
import { updateHash } from "../../../app/useHash";
import { getSecondsLeft } from "../../../services/SpotifyApi";

export default function TimerApp() {
    const [timeleft, setTime] = useState(0);
    useEffect(() => {
        const validateTime = () => {
            let tleft = getSecondsLeft();
            setTime(tleft);
            if (tleft <= 0) {
                updateHash("relogin");
            }
        };
        let intervalid = setInterval(validateTime, 900);
        validateTime();
        return () => {
            clearInterval(intervalid);
        }
    })

    return <div style={{ opacity: .5 }}><i className="fa fa-clock-o" style={{ color: "green" }}></i>&nbsp;time left: {timeleft < 60 ? timeleft + "seg" : ((timeleft / 60).toFixed(0)) + "min"}</div>;
}