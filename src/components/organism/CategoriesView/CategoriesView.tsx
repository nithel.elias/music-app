import { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { addCategories, CategoriesType, getCategories, selectCategories } from "../../../features/categories/categoriesSlice";
import AppButton from "../../atoms/AppButton/AppButton";
import CategorySection from "../CategorySection/CategorySection";

import "./style.css";


export default function CategoriesView() {
    const [status, setStatus] = useState("idle");
    const categories = useAppSelector(selectCategories);
    const dispatch = useAppDispatch();
    const requestCategories = function (page = 0) {
        if (status !== "idle") {
            // --("all ready requesting")
            return;
        }

        setStatus("running");
        getCategories(page).then((categories) => {
            setStatus("idle");
            dispatch(addCategories({ page, categories }))
        });
    };

    if (categories.list.length === 0) {
        requestCategories(0);
    }

    return (<div >
        {categories.list.map((category: CategoriesType, idx: number) => {
            return <CategorySection key={category.id} category={category} />;
        })}
        <div style={{ textAlign: "center", marginTop: "3rem" }}>
            <AppButton onclick={() => {
                requestCategories(categories.page + 1)
            }}> 
            {status === "idle" ? "Mirar más" : <i className="fa fa-spinner fa-spin"></i>}
            </AppButton>
        </div>
    </div>)
}