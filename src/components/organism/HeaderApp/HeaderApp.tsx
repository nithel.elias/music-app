import HeaderMenu from "../../molecules/HeaderMenu/HeaderMenu";
import { LogoButton } from "../../molecules/LogoButton/LogoButton";
import ProfileButton from "../../molecules/ProfileButton/ProfileButton";
import "./style.css";
export default function HeaderApp() {
    return <header className="app__header">
        <div className="app__header__container"> <LogoButton />   <HeaderMenu></HeaderMenu>  <ProfileButton></ProfileButton></div>
    </header>
}