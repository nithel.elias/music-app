import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { addTracksToPlayList, selectPlayListDic } from "../../../features/playlist/playlistSlice";
import { addTracks, getTracksFromPlayList, TrackType } from "../../../features/tracks/tracksSlice";
import CoverImage from "../../atoms/CoverImage/CoverImage";
import TrackList from "../TrackList/Tracklist";
import "./style.css";
type propsPlaylist = {
    playlist_id: string
}
export default function PlayListView({ playlist_id }: propsPlaylist) {
    const playListDicStore = useAppSelector(selectPlayListDic);

    const playListInfo = playListDicStore[playlist_id];
    const track_list: Array<string> = playListDicStore.hasOwnProperty(playlist_id) ? (playListDicStore[playlist_id].track_list || []) : [];
    const dispatch = useAppDispatch();
    (() => {
        if (track_list.length > 0) {
            return;
        }
        getTracksFromPlayList(playlist_id).then((tracks_list_response) => {
            dispatch(addTracks({ tracks: tracks_list_response }));
            dispatch(addTracksToPlayList({
                playlist_id, track_list: tracks_list_response.map((track: TrackType) => {
                    return track.id
                })
            }));
        });
    })();

    return <div className="playListView">
        <header className="playListView__header">


            <CoverImage src={playListInfo.image} alt={playListInfo.name} image_className="playListView__image" figure_className="playListView__figure"  ></CoverImage>

            <aside className="PlayListView__header__aside ">
                <small>Lista</small>
                <h1 className="PlayListView__title ellipsis-text">{playListInfo.name}</h1>
                <h3 className="PlayListView__subtitle ">
                    {playListInfo.description}
                </h3>
            </aside>
        </header>

        <section role="presentation" className="playListView__body">
            <TrackList track_list={track_list} />
        </section>

    </div>
}