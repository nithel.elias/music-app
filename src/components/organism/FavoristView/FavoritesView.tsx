import { useAppSelector } from "../../../app/hooks";
import { selectFavorites } from "../../../features/favorites/favoritesSlice";
import TrackList from "../TrackList/Tracklist";
import "./style.css";
export default function FavoritesView() {
    const favoritesStore = useAppSelector(selectFavorites);
    const track_list: Array<string> = Object.keys(favoritesStore);
   
    return <div className="favoritesView">
        <header className="favoritesView__header">

           <h1> FAVORITOS</h1>
        </header>

        <section role="presentation" className="favoritesView__body">
            <TrackList track_list={track_list} />
        </section>

    </div>
}