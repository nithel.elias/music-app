
import "./style.css";
const AppButton = (props: any) => {

    return (<a className={"app-button " + props?.className} onClick={() => {
        if (props?.onclick) {
            props.onclick();
        }
    }}>
        {props?.children}
    </a>);
};

export default AppButton;