import { useAppSelector } from "../../../app/hooks";
import { selectUser } from "../../../features/user/userSlice";
import "./style.css";
export default function ProfileCirclePic() {
    const user = useAppSelector(selectUser);
    return <img className="profile-circle-pic"
        src={user.images[0].url}
        alt="profile name" />;
}