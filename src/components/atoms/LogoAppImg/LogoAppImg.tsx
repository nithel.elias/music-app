import { APPNAME } from '../../../AppConfig';
import logo from '../../../images/logo.png';
import "./style.css";
export default function LogoAppImg() {
    return <img className="app__logo" src={logo}  style={{height:"100%"}} title={APPNAME} />
}