type propsAppLink = {
    href: string,
    className: string
};
export default function AppLink({ href, className }: propsAppLink) {
    return <a className={"app__link "+className} href={href}></a>
}