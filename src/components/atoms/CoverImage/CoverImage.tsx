import "./style.css";
type propsCoverImage = {
    figure_className: string,
    image_className: string,
    src: string, alt: string
};

export default function CoverImage({ figure_className, image_className, src, alt }: propsCoverImage) {
    return <figure className={"cover_figure " + figure_className}>
        <img src={src} alt={alt} className={"cover_image " + image_className} />
    </figure>
}