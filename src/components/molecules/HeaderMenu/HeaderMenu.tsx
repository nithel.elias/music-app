import MainNavButtons from "../MainNavButtons/MainNavButtons";
import "./style.css"
export default function HeaderMenu() {
    return <div className="app__header__menu">

        <MainNavButtons>
       
            <a href="browse">Explorar</a>
            <a href="favorites">Favoritos&nbsp;<i className="fa fa-heart red-text app_header__menu__fav-icon" ></i></a>
        </MainNavButtons>
    </div >
}