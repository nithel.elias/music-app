import { APPNAME } from "../../../AppConfig";
import LogoAppImg from "../../atoms/LogoAppImg/LogoAppImg";
import "./style.css";
export function LogoButton() {
    return <a href="/" draggable="false" aria-current="page" className="logobutton brand">
        <figure className="logobutton__figure">
            <LogoAppImg></LogoAppImg> 
        </figure>
        <span className="app-brand-title">{APPNAME}</span>
    </a>;
}