import { DOMElement, useEffect, useMemo, useRef, useState } from "react";
import CoverImage from "../../atoms/CoverImage/CoverImage";
import "./style.css";
interface propsCardItem {
    tittle: string, subtittle: string, imageUrl: string, onclick: () => void
};
function useIsInViewport(ref: any) {
    const [isIntersecting, setIsIntersecting] = useState(false);

    const observer = useMemo(
        () =>
            new IntersectionObserver(([entry]) => {
                if (entry.isIntersecting) {
                    setIsIntersecting(entry.isIntersecting);
                    observer.disconnect();// WE DISCONNECT
                }
            },
            ),
        [],
    );

    useEffect(() => {
        observer.observe(ref.current);
        return () => {
            observer.disconnect();
        };
    }, [ref, observer]);

    return isIntersecting;
}
export default function CardItem({ tittle, subtittle, imageUrl, onclick }: propsCardItem) {

    const ref1 = useRef(null);
    const isInViewport = useIsInViewport(ref1);



    return <div ref={ref1} className={"card " + (isInViewport ? " card--visible " : "")}
        onClick={() => {
            if (onclick)
                onclick();
        }} >
        <article className="card__container" >
            <aside className="card__aside">
                <CoverImage src={imageUrl} alt={tittle} image_className="card__image" figure_className="card__figure"  ></CoverImage>
            </aside>
            <header className="card__header">
                <h2 className="card__title ellipsis-text">{tittle}</h2>
                <h4 className="card__subtitle ">
                    {subtittle}
                </h4>
            </header>

        </article></div>;
}