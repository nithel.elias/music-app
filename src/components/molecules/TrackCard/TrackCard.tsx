

import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { getAudioPlayer, selectAudioPlayer, setTrack, toggleTrack } from "../../../features/audioPlayer/audioPlayerSlice";
import { addTrackToFavorite, removeTrackFromFavorite, addTrackToFavoriteOnStore, removeTrackFromFavoriteOnStore, selectFavorites } from "../../../features/favorites/favoritesSlice";
import { TrackType } from "../../../features/tracks/tracksSlice";
import CardItem from "../CardItem/CardItem";
type propsTrackCard = {
    track: TrackType
};

export default function TrackCard({ track }: propsTrackCard) {
    const favoriteStore = useAppSelector(selectFavorites);
    const audioPlayerStore = useAppSelector(selectAudioPlayer);
    const isFav = (favoriteStore.hasOwnProperty(track.id));

    const dispatch = useAppDispatch();
    const toggleTrackFavorite = () => {

        (!isFav ? addTrackToFavoriteOnStore : removeTrackFromFavoriteOnStore)(track.id)
        dispatch((!isFav ? addTrackToFavorite : removeTrackFromFavorite)({ trackid: track.id }));

    };
    const toggleSong = () => {

        toggleTrack(track.mp3).then((state: string) => {
         
            dispatch(setTrack({ track: track.mp3, state }));
            if (state === "loading") {
           
                getAudioPlayer().onPlaying(() => {
             
                    dispatch(setTrack({ track: track.mp3, state: "playing" }));
                    getAudioPlayer().onPlaying(() => { });
                });

            }
        });
    };



    let iconByPlayState = "play";

    if (audioPlayerStore.src === (track.mp3)) {
       
        iconByPlayState = ({
            idle: "play",
            ready: "play",
            loading: "spinner fa-spin",
            playing: "pause",
            failed: "exclamation-triangle"
        })[audioPlayerStore.state];
    }


    return <div className="track__cardwrapper" >
        <button className={"track__floatbutton track__floatbutton--right " + (isFav ? "" : "track__floatbutton--unfav")}
            onClick={() => toggleTrackFavorite()}>
            <i className="fa fa-heart"></i>
        </button>
        <button className="track__floatbutton track__floatbutton--center" onClick={toggleSong} >
            <i className={"fa fa-" + (iconByPlayState)}></i>
        </button>
        <CardItem
            tittle={track.name}
            subtittle={track.artists.join(" ")}
            imageUrl={track.album.image}
            onclick={toggleSong}></CardItem>
    </div>;
}