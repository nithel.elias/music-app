import { ReactNode } from "react";
import "./style.css"
type propsSubtitleAndDescription = {
    tittle: ReactNode|string, description: ReactNode|string, className: string
};
 
export default function SubtitleAndDescription({ tittle, description = "", className = "" }: propsSubtitleAndDescription) {
    return <div className={"subttile_wrapper " + (className)}>
        <h2 className="subttile_wrapper--title ellipsis-text">{tittle}</h2>
        {(description != "") ? <p className="subttile_wrapper--description ellipsis-text">{description}</p> : null}
    </div>
}