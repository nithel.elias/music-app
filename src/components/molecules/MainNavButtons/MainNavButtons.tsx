import { useHash } from "../../../app/useHash";
import "./style.css";

export default function MainNavButtons(props: any) {
    const [hash, setHash] = useHash();
    const currentHash = hash.toString().replace("#", "").replace("/", "");
    let options: any = (props?.children.map((_option: any) => ({
        href: _option.props.href,
        "inner": _option.props.children
    }))) || [];
    return <nav className="main-nav" aria-label="Main">
        <ul className="main-nav__list">
            <li>
                <a className="main-nav__link main-nav__link--historyback" onClick={() => {
                    window.history.back();
                }}>
                    <i className="fa fa-chevron-circle-left"></i>
                </a>
            </li>
            {options.map((_optionJsx: any, idx: number) => {
                return [(<li key={idx} className="main-nav__item">
                    <a href={"#/" + _optionJsx.href} className={"main-nav__link " + (currentHash == _optionJsx.href ? "active" : "")}>
                        {_optionJsx.inner}</a >
                </li>), idx % 2 == 0 ? <li key={idx + "_"}>|</li> : null]
            })}
        </ul>
    </nav>
}