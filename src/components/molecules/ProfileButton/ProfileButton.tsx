
import { useAppSelector } from "../../../app/hooks";
import { selectUser } from "../../../features/user/userSlice";
import ProfileCirclePic from "../../atoms/ProfileCirclePic/ProfileCirclePic";
import "./style.css";
export default function ProfileButton() {
    const user = useAppSelector(selectUser);
    return <a className="profile_button shiny-button" href={"/"+window.location.hash+"/profile"} >
        <figure className="profile-button-figure">
            <ProfileCirclePic />
        </figure>
        <span className="profile_button--append"> {user.display_name} </span>
    </a>
}