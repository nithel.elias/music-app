type validRoutesType = {
    paths: Array<string>,
    routes: Array<string>,
    specialpaths: Array<any>
}
const validRoutes: validRoutesType = { paths: [], routes: [], specialpaths: [] }


export const initValidRoutes = (paths: Array<string>, routes: Array<string>) => {
    validRoutes.paths = paths;
    validRoutes.routes = routes;

    validRoutes.specialpaths = routes.filter((path) => {
        return path.indexOf("*") > -1
    }).map((path) => {
        return new RegExp(path.split("/")[0] + "\/(.+)");
    });
};

export const routeApp = () => {
    let path = window.location.pathname;
    let hash = window.location.hash.replace("#/", ""); 
    if (validRoutes.paths.indexOf(path) < 0) {
        window.location.pathname = validRoutes.paths[0];
    }
    if (validRoutes.routes.indexOf(hash) < 0) {
        // validate special paths
        if (validRoutes.specialpaths && validRoutes.specialpaths.filter((pattern) => {
            return pattern.test(hash)
        }).length === 0) {
            window.location.hash = validRoutes.routes[0];
        }
    }

}