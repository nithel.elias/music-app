function encript(dataString: string) {
    return dataString;
}
function decript(encodeString: string) {
    return encodeString;
}

export default class StorageManager {
    private keystorage: string = "";
    public data: any = null;
    defaultData: any = null;
    constructor(keystorage: string, defaultData: any = null) {
        this.defaultData = defaultData;
        this.keystorage = encript(keystorage);
        this.data = JSON.parse(decript(localStorage.getItem(this.keystorage) || JSON.stringify(defaultData)));
    }
    set(newData: any) {
        this.data = {
            ...this.data,
            ...newData
        };
        localStorage.setItem(this.keystorage, encript(JSON.stringify(this.data)));
    }
    clear() {
        this.data = this.defaultData;
        localStorage.removeItem(this.keystorage);
    }
};
