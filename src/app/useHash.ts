import { useCallback, useEffect, useState } from "react";

export const useHash = () => {
    const [hash, setHash] = useState(() => window.location.hash);

    const hashChangeHandler = useCallback(() => {
        setHash(window.location.hash);
    }, []);

    useEffect(() => {
        window.addEventListener('hashchange', hashChangeHandler);
        return () => {
            window.removeEventListener('hashchange', hashChangeHandler);
        };
    }, []);

    const setNewHash = useCallback(
        (newHash: string) => {
            if (newHash !== hash) window.location.hash = newHash;
        },
        [hash]
    );

    return [hash, setNewHash];
};

export const updateHash = (newHash: string) => {
    window.location.hash = newHash;

};

export const getCleanHash = () => {
    return window.location.hash.toString().replace("#", "").replace("/", "");
}