import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import userInfoReducer from '../features/user/userSlice';
import playListReducer from '../features/playlist/playlistSlice';
import categoriesReducer from '../features/categories/categoriesSlice';
import tracksReducer from "../features/tracks/tracksSlice";
import favoritesReducer from "../features/favorites/favoritesSlice";
import audioPlayerReducer from '../features/audioPlayer/audioPlayerSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    user: userInfoReducer,
    categories: categoriesReducer,
    playlists: playListReducer,
    tracks: tracksReducer,
    favorites: favoritesReducer,
    audioplayer: audioPlayerReducer
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
